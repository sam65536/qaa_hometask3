package com.qatestlab.task3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

import java.util.Arrays;
import java.util.stream.Collectors;

public class EventHandler extends AbstractWebDriverEventListener {

    @Override
    public void beforeNavigateTo(String url, WebDriver webDriver) {
        System.out.println("Navigate to " + url);
    }

    @Override
    public void afterNavigateTo(String url, WebDriver webDriver) {
        System.out.println("Open url: " + url);
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) {
        System.out.println("Navigate back");
    }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {
        System.out.println("Current URL: " + webDriver.getCurrentUrl());
    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) {
        System.out.println("Navigate forward");
    }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {
        System.out.println("Current URL: " + webDriver.getCurrentUrl());
    }

    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) {
        System.out.println("Refresh page");
    }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {
        System.out.println("Current URL: " + webDriver.getCurrentUrl());
    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        System.out.println("Search for element: " + by.toString());
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        System.out.println("Element found successfully");
    }

    @Override
    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
        System.out.println("Click on element: " + webElement.getTagName());
    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver webDriver) {
        System.out.println("Element successfully clicked");
    }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        String value = Arrays.stream(charSequences).map(CharSequence::toString).collect(Collectors.joining());
        System.out.println((String.format("Change value of %s: %s", webElement.getTagName(), value)));
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        System.out.println(String.format("Changed element " + webElement.getTagName()));
    }

    @Override
    public void beforeScript(String script, WebDriver webDriver) {
        System.out.println("Execute script: " + script);
    }

    @Override
    public void afterScript(String script, WebDriver webDriver) {
        System.out.println("Script executed");
    }
}