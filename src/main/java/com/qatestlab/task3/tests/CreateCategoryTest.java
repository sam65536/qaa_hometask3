package com.qatestlab.task3.tests;

import com.qatestlab.task3.BaseScript;
import com.qatestlab.task3.GeneralActions;
import org.openqa.selenium.WebDriver;

public class CreateCategoryTest extends BaseScript {

    private static final String USER_NAME = "webinar.test@gmail.com";
    private static final String USER_PASSWORD = "Xcg7299bnSmMuRLp9ITw";
    private static final String NEW_CATEGORY = "TEST";

    public static void main(String[] args) {
        WebDriver driver = getConfiguredDriver();
        GeneralActions actions = new GeneralActions(driver);
        actions.login(USER_NAME, USER_PASSWORD);
        actions.waitForContentLoad();
        actions.createAndCheckNewCategory(NEW_CATEGORY);
        actions.waitForContentLoad();
        actions.tearDown();
    }
}