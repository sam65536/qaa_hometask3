package com.qatestlab.task3;

import com.qatestlab.task3.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.function.Consumer;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        driver.get(Properties.getBaseAdminUrl());
        WebElement loginField = driver.findElement(By.id("email"));
        loginField.sendKeys(login);
        WebElement passwordField = driver.findElement(By.id("passwd"));
        passwordField.sendKeys(password);
        WebElement loginButton = driver.findElement(By.cssSelector("button[type='submit']"));
        loginButton.click();
        wait.until(elementToBeClickable(By.cssSelector(".img-thumbnail")));
    }

    /**
     * Adds new category in Admin Panel.
     * @param categoryName
     */
    public void createAndCheckNewCategory(String categoryName) {
        Actions action = new Actions(driver);
        Consumer<By> hover = (By by) -> action.moveToElement(driver.findElement(by)).perform();
        hover.accept(By.id("subtab-AdminCatalog"));
        driver.findElement(By.id("subtab-AdminCategories")).click();
        WebElement createCategoryButton = driver.findElement(By.cssSelector("i.process-icon-new"));
        createCategoryButton.click();
        waitForContentLoad();
        WebElement categoryNameField = driver.findElement(By.id("name_1"));
        categoryNameField.sendKeys(categoryName);
        scrollByPage();
        WebElement saveCategoryButton = driver.findElement(By.id("category_form_submit_btn"));
        saveCategoryButton.click();
        wait.until(visibilityOf(driver.findElement(By.cssSelector("div.alert:nth-child(1)"))));
        driver.findElement(By.cssSelector(".title_box > a[href*='Orderby=name'] + *")).click();
        wait.until(visibilityOf(driver.findElement((By.xpath("//td[contains(text(),'" + categoryName + "')]")))));
    }

    private void scrollByPage() {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad() {
        wait.until(invisibilityOf(driver.findElement(By.cssSelector("#ajax_running > i:nth-child(1)"))));
    }

    public void tearDown() {
        if(driver != null) {
            driver.quit();
        }
    }
}